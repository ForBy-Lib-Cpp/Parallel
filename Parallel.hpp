//  ForBy LTD 2017
/// Parallel Core

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/
 */

#ifndef FORBY_PARALLEL_CORE_INCLUDED
#define FORBY_PARALLEL_CORE_INCLUDED

#define _forby_parallel_core_edition 0x0001

#include <thread>
#include <future>
#include <functional>
#include <type_traits>

namespace ForBy::Parallel
{

  template
  <  class U
  ,  typename F
  ,  typename... Args
  ,  using R = std::result_of<U::F>
  >
  struct ParallelFunc
  {
      ParallelFunc( &U::F _f, Args&& ...args ) noexcept
      {
          this->_f = std::mem_fn(_f);
          this->args = std::make_tuple(args);
      }

      inline std::future&& get_future() noexcept
      {
          return pr.get_future();
      }

      inline const void invoke(&U _this_ ) noexcept
      {
          try
          {
              auto r = std::invoke( _f, _this_ , _args );
              pr.set_value(r);
          }
          catch( ForBy::Exception& e)
          {
              pr.set_value(e);
          }
      }

    protected:
      auto _f;
      auto _args;
      std::promise<R> pr;
  };

  template
  <  class U
  ,  using Inherits = std::decay_t<U>
  >
  class ParallelCore
  {
  protected:

      std::unique_ptr<Inherits> _object;

      std::queue<Func_T> _queue;

      std::thread _thread;

      std::mutex _mtx;
      std::condition_variable _cv;
      std::atomic_flag busy = ATOMIC_FLAG_INIT;


      inline void __noreturn__
      thread_coffee() noexcept
      {
         while(true){

          for
          ( std::unique_lock<std::mutex> lock(_mtx)
          ; _queue.empty()
          ; _cv.wait(lock)
          ) if( kill.load() == true ) break;

          jobQueue.pop_front().invoke(*_object);

         }
      }

  public:

      ParallelCore( ParallelCore&& ) = delete;
      ParallelCore( const ParallelCore& ) = delete;
      ParallelCore( const ParallelCore&& ) = delete;

      template<typename... Args>
      inline ParallelCore( Args&& ...args)
      : _thread( this, thread_coffee )
      , cv(mtx)
      , _object( std::make_unique(
              Inherits(std::forward<Args>(args)...)
          )
      )
      {
          static_assert
          (   !std::is_class<U>::value
          ||  std::is_compount<U>::value
          ,   "ParallelCore can only be attached to classes"
          )
      };

  template
  <  typename F
  ,  typename... Args
  ,  using _r = std::result_of<F>::type,
  ,  using R = std::future<_r>
  ,  using _F = std::decay_t<F>
  >
  inline R operator->( &Inherits::_F _mf, Args&& ..._args  )
  {
      static_assert
      ( ! std::is_member_function_pointer<F>::value
      , "Template argument is not a member function."
      )

      while(busy.test_and_set(std::memory_order_acquire));

          _queue.emplace(
              ParallelFunc(_mf, std::forward<Args>(_args)... )
          );

          auto fut = _queue.back().get_future();
          _cv.notify_one();

      busy.test_and_set(std::memory_order_release);

      return fut;
  }


  inline Inherits const* operator->(&Inherits::Var)
  {
      static_assert
      ( ! std::is_member<Var>::value
      && std::is_not_function
      , "Template argument is not a member variable."
      )

      return &_object
  }


  }; // ParallelCore

  template
  < typename F
  , using R = std::result_of<F>::value
  >
  struct Parallel_Task
  {
        std::promise<R> _return;
        auto func;

        Parallel_Task( auto _func )
        : func( _func)
        {
            static_assert
            (  ! std::if_function<F>::value
            ,  "Template argument was not function"
            );
        }
  }

  /*
        template
        < typename F
        , typename... Args
        , using _r = std::result_of<F>::value,
        , using R = std::conditional
          <   std::is_void< _r >::value
          ,   void_t
          ,   std::future< _r >
        > >
        inline R run( F&& _func, Args&& ..._args )
        {
            auto _run = std::function<F> (
                          std::bind
                          (  static_cast<Inherits>(this)
                          ,  _func
                          ,  std::forward<Args>(_args)...
                          )
            );

            return static_cast<_r>( _caffeine( &_run ); );
        }
  */

} // namespace ForBy::Parallel

#endif // FORBY_PARALLEL_CORE_INCLUDED
